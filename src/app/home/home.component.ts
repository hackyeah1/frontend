import { Component, OnInit } from '@angular/core';
import { NewsService } from '@core/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoading = false;
  text: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.isLoading = true;
  }

  requestVerify(textToValid: string) {
    return this.validURL(textToValid)
      ? NewsService.verify({ type: 'link', content: textToValid })
      : NewsService.verify({ type: 'text', content: textToValid });
  }

  async checkNewsHandler() {
    const requestValue = await this.requestVerify(this.text);
    await this.router.navigate([`/verify/${requestValue.id}`]);
  }

  public validURL(str: string) {
    const pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i'
    ); // fragment locator
    return !!pattern.test(str);
  }
}
