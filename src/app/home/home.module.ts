import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { CoreModule } from '@core';
import { SharedModule } from '@shared';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    HomeRoutingModule,
    ButtonModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [HomeComponent],
})
export class HomeModule {}
