import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/i18n';
import { Shell } from '@app/shell/shell.service';
import { ListComponent } from './list.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'list', component: ListComponent, data: { title: extract('List') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ListRoutingModule {}
