import { Injectable } from '@angular/core';
import { NewsService, NewsPayload } from '@app/@core/http';

@Injectable({
  providedIn: 'root',
})
export class ListService {
  public list: NewsPayload[];
  constructor() {}

  public async getList() {
    this.list = await NewsService.news2();
  }

  public async getUnconfirmed() {
    this.list = await NewsService.news2({ opinion: 'unconfirmed' });
  }
}
