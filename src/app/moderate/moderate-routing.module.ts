import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/i18n';
import { Shell } from '@app/shell/shell.service';
import { ModerateComponent } from './moderate.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'moderate/:id', component: ModerateComponent, data: { title: extract('Zweryfikuj') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ModerateRoutingModule {}
