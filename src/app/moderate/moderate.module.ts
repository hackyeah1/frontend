import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'primeng/button';

import { ModerateRoutingModule } from './moderate-routing.module';
import { ModerateComponent } from './moderate.component';

@NgModule({
  imports: [CommonModule, TranslateModule, ModerateRoutingModule, ButtonModule],
  declarations: [ModerateComponent],
})
export class ModerateModule {}
