import { Component, OnInit, Input } from '@angular/core';
import { NewsPayload, NewsService } from '@app/@core/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-moderate',
  templateUrl: './moderate.component.html',
  styleUrls: ['./moderate.component.scss'],
})
export class ModerateComponent implements OnInit {
  public post: NewsPayload;
  public lock: boolean = false;
  public done: boolean = false;
  private id: string;

  constructor(private route: ActivatedRoute) {}

  async ngOnInit() {
    this.route.params.subscribe(async (params) => {
      this.id = params.id;
      this.post = await NewsService.news({ id: this.id });
    });
  }

  public get content() {
    return this.post ? this.post.content : null || null;
  }

  public async prawda() {
    this.lock = true;
    await NewsService.makeItTruth({ id: this.id });
    this.lock = false;
    this.done = true;
  }

  public async fake() {
    this.lock = true;
    await NewsService.makeItFake({ id: this.id });
    this.lock = false;
    this.done = true;
  }
}
