import { Component, OnInit, Input } from '@angular/core';

import { environment } from '@env/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService, NewsPayload } from '@app/@core/http';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {
  @Input() state: string = null;
  news: NewsPayload = {} as NewsPayload;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      NewsService.news({ id: params['id'] })
        .then((x) => {
          this.news = x;
          if (this.news.content.length > 480) {
            this.news.content.slice(0, 480);
            this.news.content += '...';
          }
          this.state = this.news.opinion;
        })
        .catch((x) => {
          this.state = 'unconfirmed';
        });
    });
  }
}
