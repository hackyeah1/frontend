import { UnknownComponent } from './unknown/unknown.component';
import { NewsComponent } from './news/news.component';
import { FakeComponent } from './fake/fake.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';

import { VerifyRoutingModule } from './verify-routing.module';
import { VerifyComponent } from './verify.component';
import { ButtonModule } from 'primeng/button';
import { ClipboardModule } from 'ngx-clipboard';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [VerifyRoutingModule, CommonModule, ButtonModule, ClipboardModule, InputTextModule, FormsModule],
  declarations: [VerifyComponent, FakeComponent, NewsComponent, UnknownComponent],
})
export class VerifyModule {}
