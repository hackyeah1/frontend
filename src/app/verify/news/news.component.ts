import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
})
export class NewsComponent implements OnInit {
  @Input() content: string;
  url: string = window.location.href;
  linkCopied = false;

  constructor() {}

  ngOnInit() {}

  copyLink() {
    this.linkCopied = true;
  }
}
