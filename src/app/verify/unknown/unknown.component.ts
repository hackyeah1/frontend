import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unknown',
  templateUrl: './unknown.component.html',
  styleUrls: ['./unknown.component.css'],
})
export class UnknownComponent implements OnInit {
  public emailSubmitted = false;
  newsletterMail: string;

  constructor() {}

  ngOnInit() {}

  onEmailSubmit() {
    this.emailSubmitted = true;
  }
}
