import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/i18n';
import { Shell } from '@app/shell/shell.service';
import { VerifyComponent } from './verify.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'verify/:id', component: VerifyComponent, data: { title: extract('Verify') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class VerifyRoutingModule {}
