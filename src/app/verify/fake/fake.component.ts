import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fake',
  templateUrl: './fake.component.html',
  styleUrls: ['./fake.component.css'],
})
export class FakeComponent implements OnInit {
  @Input() content: string;
  url: string = window.location.href;
  linkCopied = false;

  constructor() {}

  ngOnInit() {}

  copyLink() {
    this.linkCopied = true;
  }
}
